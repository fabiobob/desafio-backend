'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URLs and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.1/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route');

Route.post('/create', 'AuthController.create').validator('User')
Route.post('/login', 'AuthController.login')
Route.get('/users', 'AuthController.index')

Route.group(() => {
  Route.resource('stores', 'StoreController').apiOnly().validator('Store');
});

Route.post('/transfers', 'TransferController.create')

Route.post('/login', 'AuthController.login')
