'use strict'

const User = use('App/Models/User')
const { validate } = use('Validator')

class AuthController {
  async create({ params, request }){
    const rules = {
      username: 'required|username|unique:users,username',
      email: 'required|email|unique:users,email',
      cpf: 'required|cpf|unique:users,cpf',
      password: 'required'
    }

    const data = request.only(['username', 'cpf', 'email', 'password']);
    const validation = await validate(data, rules)

    if (validation.fails()) {
      return validation.messages()
    }

    const user = await User.create(data);

    return user
  }

  async login({ request, auth }){
    const { email, password } = request.all();

    const token = await auth.attempt(email, password);

    return token;
  }

  async index () {
    const users = await User.all()

    return users;
  }
}

module.exports = AuthController
