'use strict'

const Store = use('App/Models/Store');
const Database = use('Database')
const { validate } = use('Validator');

class TransferController {
  async create({ params, request }){
    const rules = {
      origin: 'required',
      destiny: 'required',
      amount: 'required'
    }

    const data = request.only(['id', 'origin', 'destiny', 'amount']);
    const validation = await validate(data, rules)

    if (validation.fails()) {
      return validation.messages()
    }

    const { origin, destiny, amount } = request.all();

    console.log("Valor de destiny = "+destiny);

    await Database
      .table('stores')
      .where('document', destiny)
      .increment('amount', amount);

    await Database
      .table('stores')
      .where('document', origin)
      .decrement('amount', amount)
  }
}

module.exports = TransferController
