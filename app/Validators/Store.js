'use strict'

class Store {
  get rules () {
    return {
      document: 'required|document|unique:document',
      amount: 'required|amount'
    }
  }

  get messages () {
    return {
      'document.required': 'You must provide a document.',
      'document.unique': 'This document is already registered.',
      'amount.required': 'You must provide a amount.'
    }
  }
}

module.exports = Store
