'use strict'

class User {
  get rules () {
    return {
      username: 'required|username|unique:username',
      email: 'required|email|unique:users,email',
      cpf: 'required|cpf|unique:users,cpf',
      password: 'required'
    }
  }

  get messages () {
    return {
      'email.required': 'You must provide a email address.',
      'email.email': 'You must provide a valid email address.',
      'email.unique': 'This email is already registered.',
      'cpf.required': 'You must provide a cpf.',
      'cpf.unique': 'This cpf is already registered.',
      'password.required': 'You must provide a password'
    }
  }
}

module.exports = User
